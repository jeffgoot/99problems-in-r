# 99 Problems in R
This project is my attempt to implement the [99 Problems in Prolog](https://sites.google.com/site/prologsite/prolog-problems) in R as a way to learn the [R programming language](https://www.r-project.org/about.html), [RStudio](https://www.rstudio.com/) and [testthat](https://github.com/hadley/testthat) for my [Statistics class](http://www.chicagobooth.edu/programs/exec-mba/academics/curriculum/foundations) at [Chicago Booth](http://www.chicagobooth.edu/programs/exec-mba). 

For a far more complete and documented implementation, you're better off going to [Swanand's 99-Problems-R](https://github.com/saysmymind/99-Problems-R).
